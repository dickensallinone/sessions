import { useEffect, useState } from "react";
import CourseCard from "./CourseCard";
import CourseSearch from "./CourseSearch";
import CourseSearchByPrice from "./CoursePriceSearch";

export default function UserView(props){    
    const {coursesData} = props;
    const [courseData,setCourseData] = useState(null);

    useEffect(()=>{
        setCourseData(coursesData.map(course_item => {
            return (
                course_item.isActive && <CourseCard key={course_item._id} course={course_item}/>
            )
        }))
    },[coursesData])
    
    return (
        <>
            <h1>Courses</h1>
            <CourseSearchByPrice/>
            <CourseSearch/>
			{courseData }
        </>
    )
}
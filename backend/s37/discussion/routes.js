let http = require('http');

const port = 4000;

const app = http.createServer((req,res)=>{
    if(req.url === '/greeting'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end('Hello Again')
    }
    else if(req.url == '/homepage'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end('Welcome to the homepage')
    }

})

app.listen(4000);

console.log(`Server now accessible at localhost: ${port}.`);




let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999
}

console.log("Result from creating object using initializers/object literal");
console.log(cellphone);
console.log(typeof cellphone);

function Laptop(name,manufactureDate) {
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating object using constructor function");
console.log(laptop);

let laptop2 = new Laptop('Macbook Air', 2020);
console.log("Result from creating object using constructor function");
console.log(laptop2);

console.log("Result from square bracket notation: " + laptop2['name'])
console.log("Result from square bracket notation: " + laptop2.name);

let array = [laptop,laptop2];

console.log(array[0]['name'])
console.log(array[0].name)

let car = {};

car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

car['manufacturing date'] = 2019;

console.log(car['manufacturing date']);
console.log(car['Manufacturing Date']);

console.log("Result from adding properties using bracket notation: ");
console.log(car);

delete car['manufacturing date'];
console.log('Result from deleting properties: ');
console.log(car);

car.names = 'Honda Civic Type R';
console.log("Result from reassigning properties: ");
console.log(car);


let person = {
    name: "Barbie",
    greet: function(){
        console.log('Hello! My name is '+ this.name);
    }
}

console.log(person);
console.log('Result from object methods:');

person.greet();

person.walk = function(){
    console.log(this.name + ' waled 25 steps forward');
}

person.walk();

let friend = {
    name: "Ken",
    address: {
        city: "Austin",
        state: 'Texas',
        country: 'USA'
    },
    email: ['ken@gmail.com','ken@mail.com'],
    introduce: function(object) {
		console.log('Nice to meet you ' + object.name + " I am " + this.name + ' from ' + this.address.city + ' ' + this.address.state);
	}
}

friend.introduce(person);

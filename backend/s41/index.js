const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config()
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin1234@b303-arboiz.p20uafb.mongodb.net/?retryWrites=true&w=majority",{
    useNewUrlParser:true,
    useUnifiedTopology: true

});

mongoose.connection.on('error',()=>console.log('Connection error:('));
mongoose.connection.once('open',()=>console.log('Connected to MongoDB!'));


app.use(express.json());
app.use(express.urlencoded({extended: true}));

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema);

app.post('/tasks',(req,res)=>{
    Task.findOne({name: req.body.name}).then((result,error)=>{
        if(result != null && result.name == req.body.name){
            return res.send('Duplicate task found')
        } else {
            let newTask = new Task({
                name: req.body.name
            });

            newTask.save().then((savedTask, error)=>{
                if(error){
                    return res.send({
                        message: error.message
                    })
                }

                return res.send(201,'New Task created!');
            })
        }
    })
})

app.get('/tasks',(req,res)=>{
    Task.find({}).then((result,error)=>{
        if(error){
            return res.send({
                message: error.message
            })
        }

        return res.status(200).json({
            tasks:result
        })
    })
})

app.listen(port, ()=> console.log(`Server is running at port ${port}`))

module.exports = app;
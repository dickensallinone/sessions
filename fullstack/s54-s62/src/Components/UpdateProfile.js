import React, { useState } from 'react';
import { Form,Button } from 'react-bootstrap';

export default function UpdateProfile({fetchProfileData}) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobilePhone, setMobilePhone] = useState('');
  const [message, setMessage] = useState('');

  const handleUpdate = async (e) => {
    e.preventDefault();

    const jwtToken = localStorage.getItem('token'); // Replace with the actual JWT token
    const apiUrl = `${process.env.REACT_APP_API_URL}/users/profile`; // Replace with the API endpoint for reset password
    
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${jwtToken}`,
    };

    const body = JSON.stringify({
      firstName,
      lastName,
      mobilePhone,
    });

    try {
      const response = await fetch(apiUrl, {
        method: 'PUT',
        headers,
        body,
      });

      const data = await response.json();

      if (response.ok) {
        setMessage('Profile updated successfully!');
        fetchProfileData();
      } else {
        setMessage(data.error || 'Profile update failed.');
      }
    } catch (error) {
      setMessage('An error occurred. Please try again later.');
    }

    setFirstName('');
    setLastName('');
    setMobilePhone('');
  };

  return (

    <Form onSubmit={handleUpdate}>
          <h2>Update Profile</h2>
           <Form.Group controlId="firstName">
               <Form.Label>First Name</Form.Label>
               <Form.Control 
                   type="text" 
                   placeholder="Enter First Name"
                   value={firstName}
                   onChange={(e) => setFirstName(e.target.value)}
                   required
               />
           </Form.Group>

           <Form.Group controlId="lastName">
               <Form.Label>Last Name</Form.Label>
               <Form.Control 
                   type="text" 
                   placeholder="Enter Last Name"
                   value={lastName}
                   onChange={(e) => setLastName(e.target.value)}
                   required
               />
           </Form.Group>

           <Form.Group controlId="mobileNo">
               <Form.Label>Mobile No:</Form.Label>
               <Form.Control 
                   type="number" 
                   placeholder="Enter Phone No."
                   value={mobilePhone}
                   onChange={(e) => setMobilePhone(e.target.value)}
                   required
               />
           </Form.Group>
           {message && <div className="mt-3 alert alert-info">{message}</div>}
          
               <Button variant="primary" type="submit" id="submitBtn">
                   Submit
               </Button>
           
       </Form>     
       
    
  );
};



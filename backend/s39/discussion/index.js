console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.json()))
.then(posts => console.log(posts));

async function fetchData(){
    let result;
    let finalResult;
    try {
        result = await fetch('https://jsonplaceholder.typicode.com/post');
        if(!result.ok) {
            console.log('error')
        }
        finalResult = await result.json();

    }catch(error){
        console.log(error)
    }

    console.log(finalResult);
}

fetchData();


fetch('https://jsonplaceholder.typicode.com/posts',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body:JSON.stringify({
        title: "New Post",
        body: 'Hello World',
        userId: 2
    })
})
.then(response => response.json())
.then(created_post => console.log(created_post));



fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body:JSON.stringify({
        title: "Updated Post"
    })
})
.then(response => response.json())
.then(updated_post => console.log(updated_post));



fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method: 'DELETE'
})
.then(response => response.json())
.then(deleted_post => console.log(deleted_post));




fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(response => response.json())
.then(filtered_post => console.log(filtered_post));


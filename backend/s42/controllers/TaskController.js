const Task = require('../models/Task');

module.exports.getAllTasks = (res)=>{
    return Task.find({}).then((result,error)=>{
        if(error){
            return res.send({
                message: error.message
            })
        }

        return res.status(200).json({
            tasks:result
        })
    })
}

module.exports.createTask = (request_body)=>{
    return Task.findOne({name: request_body.name}).then((result,error)=>{
        if(result != null && result.name == request_body.name){
            return res.send('Duplicate task found')
        } else {
            let newTask = new Task({
                name: request_body.name
            });

            newTask.save().then((savedTask, error)=>{
                if(error){
                    return res.send({
                        message: error.message
                    })
                }

                return res.send(201,'New Task created!');
            })
        }
    })
}
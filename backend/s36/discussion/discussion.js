
db.fruits.aggregate([{$match:{onSale:true}},{$group: {_id: "$supplier_id",total:{$sum:"$stock"}}}])


db.fruits.aggregate([{$match:{onSale:true}},{$group: {_id: "$supplier_id",avgStocks:{$avg:"$stock"}}},{$project: {_id:0}}])


db.fruits.aggregate([
    {$match:{
        onSale: true
    }},
    {$group:{
        
            _id: '$supplier_id',
            maxPrice:{
                $max: '$price'
            
    }}},
    {$sort:{
        maxPrice:-1
    }}
])
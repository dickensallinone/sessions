import {Button, Row, Col} from 'react-bootstrap';

export default function Banner(props){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{props.header}</h1>
				<p>{props.content}</p>
				<Button variant="primary" href={props.refRoute}>{props.buttonLabel}</Button>
			</Col>
		</Row>
	)
}
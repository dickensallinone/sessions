const express = require('express');
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.listen(port, ()=> console.log(`Server is running at port ${port}`));

app.get('/',(req,res)=>{
    res.send('Hello World!');
})

app.post('/greeting',(req,res)=>{
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
})

let users = [];

app.post('/register',(req,res)=>{
    if(req.body.username !== '' && req.body.password !==''){
        users.push(req.body);
        res.send(`User ${req.body.username} has successfully been registered`)
    }

    res.send('Please INPUT both username and password')
})

app.get('/users',(req,res)=>{
    res.send(users);
})
module.exports = app;
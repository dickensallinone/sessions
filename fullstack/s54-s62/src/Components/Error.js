import Banner from "./Banner";
export default function Error() {
    return (
        <Banner header="404- Page not found" 
        content="The page you are looking for cannot be found"
        refRoute="/"
        buttonLabel="Back Home"
        />
    )
}
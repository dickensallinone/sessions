import { useContext, useEffect, useState } from "react";
import { Col,Row } from "react-bootstrap"
import UserContext from "../UserContext";
import { Navigate } from 'react-router-dom';
import ResetPassword from "../components/ResetPassword";
import UpdateProfile from "../components/UpdateProfile";

export default function Profile(){
    const {user} = useContext(UserContext);
    const [firstName,setFirstName] = useState('');
    const [lastName,setLastName] = useState('');
    const [email,setEmail] = useState('');
    const [mobileNo,setMobileNo] = useState('');

    function fetchProfileData(){
        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.json()).then(data=>{
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setMobileNo(data.mobileNo);
        });
    }
    useEffect(()=>{
        fetchProfileData();
    },[])
    return (

        (user.access === null) ? 
		<Navigate to="/"/> :
        <>
        <Row>
            <Col className="p-5 bg-primary text-light">
                <h1 className="display-2">Profile</h1>
                <p className="display-3">{firstName} {lastName}</p>
                <hr></hr>
                
                <p>Contacts</p>
                <ul>
                    <li>Email: {email}</li>
                    <li>Mobile No: {mobileNo}</li>
                </ul>
            </Col>
        </Row>

        <Row className="pt-4 mt-4">
            <Col>
                <UpdateProfile fetchProfileData={fetchProfileData}/>
            </Col>
        </Row>

        <Row className="pt-4 mt-4">
            <Col>
                <ResetPassword/>
            </Col>
        </Row>
        </>
        
    )
}
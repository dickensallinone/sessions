const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const app = express();
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
mongoose.connect("mongodb+srv://RupertRamos09:admin123@zuitt-batch224.re3gm0t.mongodb.net/course-booking-API?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

let database = mongoose.connection;

database.on('error',()=>console.log('Connection error:('));
database.once('open',()=>console.log('Connected to MongoDB!'));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use('/users/',userRoutes);
app.use('/courses/',courseRoutes);



app.listen(process.env.PORT || port,()=>{
    console.log(`Booking System API is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;
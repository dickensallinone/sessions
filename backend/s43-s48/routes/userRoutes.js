const express = require('express');
const router = express.Router();
const auth = require('../auth');
const UserController = require('../controllers/UserController.js');

router.post('/check-email',(req,res)=>{
    UserController.checkEmailExists(req.body).then(result =>{
        res.send(result)
    })
})

router.post('/register',(req,res)=>{
    UserController.registerUser(req.body).then(result =>{
        res.send(result)
    })
})

router.post('/login',(req,res)=>{
    UserController.loginUser(req,res);
})

router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/enroll', auth.verify, (request, response) => {
	UserController.enroll(request, response);
})

router.get('/get', auth.verify, (request, response) => {
	UserController.getEnrollments(request, response);
})




module.exports = router;
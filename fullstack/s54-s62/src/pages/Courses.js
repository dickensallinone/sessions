import { useEffect, useState,useContext } from 'react';
import UserContext from '../UserContext';
import UserView from '../components/UserView.js';
import AdminView from '../components/AdminView';
export default function Courses(){
	const {user} = useContext(UserContext);
	const [courses,setCourses] = useState([]);
	
	// useEffect(()=>{
	// 	fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
	// 	.then(res => res.json())
	// 	.then(data=>{
			
	// 		setCourses(data)
	// 	})
	// },[])

	function fetchData(){
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data=>{
			
			setCourses(data)
		})
	}

	useEffect(()=>{
		fetchData();
	},[])

	return(
		
		
			(!user.isAdmin) ? <UserView coursesData={courses}/>
			: <AdminView coursesData={courses} fetchData={fetchData}/>
			
	)
}
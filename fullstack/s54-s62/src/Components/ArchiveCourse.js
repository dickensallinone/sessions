import { Button} from "react-bootstrap"
import Swal from 'sweetalert2';

export default function ArchiveCourse({courseId,courseActive,fetchData}){
    function reusableFunction(isActiveValue){
        const routeDef = courseActive ? 'archive': 'activate';
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/${routeDef}`,{
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: isActiveValue
            })
        })
		.then(res => res.json())
		.then(data=>{  
         if(data) {
            Swal.fire({
                title: `Course is ${courseActive ? 'Archived': 'Activated'} Successfully`,
                icon: 'success',
                text: 'Course Successfully Updated'
               })
            fetchData();
         }

        else{
            Swal.fire({
                title: 'Update failed',
                icon: 'error',
                text: 'Please try again!'
               })
        }
		}) 
    }
    function archiveToggle(){
        reusableFunction(false)
    }

    function activateToggle(){

        reusableFunction(true)
    }

    return(

        <Button variant={courseActive? "danger":"success"}
             onClick={courseActive ? archiveToggle : activateToggle}
            >{
                courseActive? "Archive":"Activate"
            }</Button>
    )
}
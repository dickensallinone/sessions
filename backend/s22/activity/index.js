

// 1. Create a function called addNum which will be able to add two numbers.
// 		- Numbers must be provided as arguments.
// 		- Return the result of the addition.
	   
// 	   Create a function called subNum which will be able to subtract two numbers.
// 	    - Numbers must be provided as arguments.
// 	    - Return the result of subtraction.

// 	    Create a new variable called sum.
// 	     - This sum variable should be able to receive and store the result of addNum function.

// 	    Create a new variable called difference.
// 	     - This difference variable should be able to receive and store the result of subNum function.

// 	    Log the value of sum variable in the console.
// 	    Log the value of difference variable in the console.

function addNum(a,b) {
    const sum = a + b;
    return sum;
}

console.log('Displayed sum of 5 and 15');
console.log(addNum(5,15));


function subNum(a,b) {
    const difference = a - b;
    return difference;
}

console.log('Displayed difference of 20 and 5');
console.log(subNum(20,5));

// 2. Create a function called multiplyNum which will be able to multiply two numbers.
// 		- Numbers must be provided as arguments.
// 		- Return the result of the multiplication.

// 		Create a function divideNum which will be able to divide two numbers.
// 		- Numbers must be provided as arguments.
// 		- Return the result of the division.

// 		Create a new variable called product.
// 		 - This product variable should be able to receive and store the result of multiplyNum function.

// 		Create a new variable called quotient.
// 		 - This quotient variable should be able to receive and store the result of divideNum function.

// 		Log the value of product variable in the console.
// 		Log the value of quotient variable in the console.

function multiplyNum(a,b) {
    const product = a * b;
    return product;
}

console.log('Displayed product of 50 and 10:');
console.log(multiplyNum(50,10));

function divideNum(a,b) {
    const quotient = a / b;
    return quotient;
}

console.log('Displayed quotient of 50 and 10:');
console.log(divideNum(50,10));

// 3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
// 		- a number should be provided as an argument.
// 		- look up the formula for calculating the area of a circle with a provided/given radius.
// 		- look up the use of the exponent operator.
// 		- return the result of the area calculation.

// 		Create a new variable called circleArea.
// 		- This variable should be able to receive and store the result of the circle area calculation.
// 		- Log the value of the circleArea variable in the console.

function getCircleArea(a){
    const circleArea = Math.PI * (a**2);
    return circleArea;
}

console.log('The result of getting the area of a circle with 15 radius:');
console.log(getCircleArea(15));


// 4. Create a function called getAverage which will be able to get total average of four numbers.
// 		- 4 numbers should be provided as an argument.
// 		- look up the formula for calculating the average of numbers.
// 		- return the result of the average calculation.
		
// 		Create a new variable called averageVar.
// 		- This variable should be able to receive and store the result of the average calculation
// 		- Log the value of the averageVar variable in the console.

function getAverage(a,b,c,d) {
    const averageVar = a + b + c + d ;
    return averageVar / 4;
}

console.log('The average of 20,40,60 and 80:');
console.log(getAverage(20, 40, 60, 80));

// 5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 		- this function should take 2 numbers as an argument, your score and the total score.
// 		- First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 		- Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 		- return the value of the variable isPassed.
// 		- This function should return a boolean.

// 		Create a global variable called outside of the function called isPassingScore.
// 			-This variable should be able to receive and store the boolean result of the checker function.
// 			-Log the value of the isPassingScore variable in the console.

let isPassingScore
function checkIfPassed(a,b) {
    let isPassed = false;
    if(a / b *100 > 75) {
        isPassed = true;
    }
    return isPassingScore = isPassed;
}

console.log('Is 38/50 a passing score?');
console.log(checkIfPassed(38,50));



try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}
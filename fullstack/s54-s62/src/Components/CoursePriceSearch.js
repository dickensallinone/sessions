import React, { useState } from 'react';

export default function CourseSearchByPrice() {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [message, setMessage] = useState('');

  const handleSearch = async (e) => {
    e.preventDefault();
    const apiUrl = `${process.env.REACT_APP_API_URL}/courses/searchByPrice`; 
    
    const headers = {
      'Content-Type': 'application/json'
    };

    const body = JSON.stringify({
        minPrice:minPrice,
        maxPrice:maxPrice,
        });

    try {
      const response = await fetch(apiUrl, {
        method: 'POST',
        headers,
        body,
      });

      const data = await response.json();
    
      if (response.ok) {
       
        setSearchResults(data.courses);
   
        setMessage('');
      } else {
        setSearchResults([]);
        setMessage(data.error || 'No results found.');
      }
    } catch (error) {
      setMessage('An error occurred. Please try again later.');
    }
  };

  return (
    <div className="container mt-5">
      <h2>Course Search by Price</h2>
      <form onSubmit={handleSearch}>
        <div className="form-group">
          <label htmlFor="minPrice">Min Price</label>
          <input
            type="number"
            id="minPrice"
            className="form-control"
            value={minPrice}
            onChange={(e) => setMinPrice(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="maxPrice">Max Price</label>
          <input
            type="number"
            id="maxPrice"
            className="form-control"
            value={maxPrice}
            onChange={(e) => setMaxPrice(e.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">Search</button>
      </form>
      {message && <div className="mt-3 alert alert-info">{message}</div>}
      {searchResults?.length > 0 && (
        <div className="mt-3">
          <h4>Search Results:</h4>
          <ul className="list-group">
            {searchResults.map((course, index) => (
              <li key={index} >{course.name}</li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};


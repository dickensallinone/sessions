let grades = [98.5, 94.3,89.2, 90.1];

let computer_brands = ["Acer","Asus","Lenovo",
"Neo", "Redfox", "Gateway", "Toshiba",
"Lenovo", "Fujitsu"];

let mixed_array = [12,"Asus",null, undefined,{}]

let my_tasks = [
    "drink html",
    "eat javascript",
    "inhale CSS",
    "bake sass"
];

console.log("Array before reassignment:");

console.log(my_tasks);

my_tasks[0] = "run hello world";

console.log("Array after reassignment:");
console.log(my_tasks);

console.log(computer_brands[1]);
console.log(grades[3]);

let index_of_last_element = computer_brands.length -1;
console.log(computer_brands[index_of_last_element]);

let fruits = ["Apple","Orange","Kiwi",
"Passionfruit"];

console.log('Current array:');
console.log(fruits);

fruits.push("Mango");

console.log("Updaed array after push method:");
console.log(fruits);

let removed_fruit = fruits.pop();

console.log("Updated array after pop method:");
console.log(fruits);
console.log("Removed fruit: " + removed_fruit);

console.log('Current array:');
console.log(fruits);

fruits.push("Mango");

console.log("Updated array after push method:");
console.log(fruits);

console.log('Current array:');
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method:");
console.log(fruits);

console.log('Current array:');
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method:");
console.log(fruits);

console.log('Current array:');
console.log(fruits);

fruits.splice(1,2, "Lime", 'Cherry');

console.log("Updated array after splice method:");
console.log(fruits);

console.log('Current array:');
console.log(fruits);

fruits.sort();

console.log("Updated array after sort method:");
console.log(fruits);

// 

let index_of_lenovo = computer_brands.indexOf("Lenovo");

console.log("The index of lenovo is: " + index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log('The index of lenovo starting from the end of the array is: '+
index_of_lenovo_from_last_item);

const hobbies = [
    "Gaming", "Running",
    "Cheating", "Cycling",
    "Writing"
];

let sliced_array_from_hobbies = hobbies.slice(2);
console.log(sliced_array_from_hobbies);
console.log(hobbies);

let sliced_array_from_hobbies_B = hobbies.slice(2,4);
console.log(sliced_array_from_hobbies_B);
console.log(hobbies);

let sliced_array_from_hobbies_C = hobbies.slice(-2);
console.log(sliced_array_from_hobbies_C);
console.log(hobbies);

// 
let string_array = hobbies.toString();
console.log(string_array);

let greeting = ["hello","world"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

console.log(hobbies.join(" - "));

hobbies.forEach((hobby)=>{
    console.log(hobby);
})

let numbers_list = [1,2,3,4,5];

let numbers_map = numbers_list.map((num)=>{
    return num * 2;
})

console.log(numbers_map);

let filtered_numbers = numbers_list.filter((num)=>num <3);

console.log(filtered_numbers);






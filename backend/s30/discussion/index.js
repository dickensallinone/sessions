console.log('ES6 Updates')

const firstNum =  8**2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

let nameKo = "Karl";

let message = 'Hello ' + nameKo +'! Welcome to Programming';
console.log('Message without template literals '+ message);

message = `Hello ${nameKo}! Welcome to Programming`;
console.log(`Message without template literals: ${message}`);

const anotherMessage = `
${nameKo} attended a Math Competition.
He won it by solving the problem 8 **2 with the answer of ${firstNum}`;

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings amount is: ${principal * interestRate}`);

const fullName = ['Juan',"Dela","Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

const [firstName,middleName,lastName] = fullName;
console.log(`Hello ${firstName} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);


const person = {
    givenName: 'Jane',
    maidenName: 'Dela',
    familyName: 'Cruz'
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

const {givenName,familyName,maidenName} = person;

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`);


function getFullName({givenName,maidenName,familyName}){
    console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person)

const hello = ()=>{
    console.log('Hello World!');
}

hello()

function printFullName(fName,mName,lName){
    console.log(fName + ' ' + mName + ' '+lName );
}

printFullName('John', 'D','Smith');

function printFullName(fName,mName,lName){
    console.log(`${fName} ${mName} ${lName}`);
}

printFullName('John', 'D','Smith');

const students = ['John','Jane','Judy'];

students.forEach(function(student){
    console.log(`${student} is a student.`);
})

students.forEach((student)=>{
    console.log(`${student} is a student.`);
})

function add(x,y){
    return x+y;
}

const add = (x,y) => x + y;

let total = add(2,5);

console.log(total);

const greet = (name = 'User') => {
    return `Good Afternoon ${name}`
}

console.log(greet());
console.log(greet('Judy'));

class Car {
    constructor(brand,name,year){
        this.brand = brand;
        this.name= name;
        this.year = year;
    }
}

const fordExplorer = new Car();

fordExplorer.brand =  "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

const toyotaVios = new Car("Toyota", "Vios", 2018);

console.log(toyotaVios);





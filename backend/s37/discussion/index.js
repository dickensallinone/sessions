let http = require("http")

http.createServer((req,res)=>{
    res.writeHead(200,{
        'Content-Type':'text/plain'
    });

    res.end('Hello World');
}).listen(4000);

console.log('Server is running at 4000');
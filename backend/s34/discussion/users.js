db.users.insertMany([
    {
        "firstName": "Jane",
        "lastName": "Doe"
    },{
        "firstName": "Joseph",
        "lastName": "Doe"
    }
])

db.users.updateMany(
    {
        "lastName": "Doe"
    },
    {
        $set: {
            "firstName": "Mary"
        }
    }

)
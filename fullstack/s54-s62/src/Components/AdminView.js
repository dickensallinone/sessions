import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import EditCourse from "./EditCourse.js";
import ArchiveCourse from "./ArchiveCourse.js";

export default function AdminView({coursesData,fetchData}) {

    const [course,setCourse] = useState(null);

    useEffect(()=>{
        setCourse(coursesData.map(course_item=>{
           return( <tr key={course_item._id}>
            <td>{course_item._id}</td>
            <td>{course_item.name}</td>
            <td>{course_item.description}</td>
            <td>{course_item.price}</td>
            <td className={`text-${course_item.isActive ? "success":"danger"}`}>Available</td>
            <td>
                <EditCourse course={course_item._id} fetchData={fetchData}/> 
            </td>
            <td>
        
                <ArchiveCourse courseId={course_item._id} 
                  courseActive={course_item.isActive}
                  fetchData={fetchData}
                />
            </td>
        </tr>)
        }))
    },[coursesData,fetchData  ])
    return (
      <>
      <h1 className="text-center">Admin Dashboard</h1>
        <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan={2}>Action</th>
          </tr>
        </thead>
        <tbody>
         {course}
        </tbody>
      </Table>
      </>
    );
  }
const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController');

router.post('/',(req,res)=>{
    TaskController.createTask(req.body).then(result=>{
        res.send(result);
    })
})

router.get('/',(req,res)=>{
    TaskController.getAllTasks(res).then(result=>{
        res.send(result);
    })
})
module.exports = router;
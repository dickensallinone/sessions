import { useEffect, useState } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router} from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar.js';
import Courses from './pages/Courses.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js'
import Register from './pages/Register.js';
import Error from './components/Error.js';
import CourseView from './pages/CourseView.js';
import AddCourse from './pages/AddCourse.js';
import Profile from './pages/Profile.js';
import './App.css';
import { UserProvider } from './UserContext.js';

// The 'App.js' component is where we usually import other custom components.
// Note: When putting two or more components together, you have to use a container for it to work properly.
function App() {

    const [user,setUser] = useState({
       id: null,
       isAdmin: null
    })

    const unsetUser =()=> localStorage.clear();

    useEffect(()=>{
      console.log(user);
      console.log(localStorage);
    },[user])

    useEffect(()=>{
      fetch('http://localhost:4000/users/details',{
        headers:{
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      }).then(res => res.json()).then(data => {
        if(data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }

        else {
          setUser({
            id: null,
            isAdmin: null
         })
        }
      })
    },[])
  
  return (
    <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <Container>
              <AppNavbar/>
              
              <Routes>
                  <Route path="/" element={<Home />} /> 
                  <Route path="/courses" element={<Courses />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/profile" element={<Profile />} />
                  <Route path="/courses/:courseId" element={<CourseView />} />
                  <Route path="/addCourse" element={<AddCourse />} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="*" element={<Error/>} />
              </Routes>
          </Container>
          </Router>
    </UserProvider>
      
      
  );
}

export default App;

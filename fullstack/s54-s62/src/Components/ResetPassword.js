import { useState } from 'react';

export default function ResetPassword (){
  const [newPassword, setNewPassword] = useState('');
  const [message, setMessage] = useState('');
  
  const handleSubmit = async (e) => {
    e.preventDefault();

    const jwtToken = localStorage.getItem('token'); // Replace with the actual JWT token
    const apiUrl = `${process.env.REACT_APP_API_URL}/users/reset-password`; // Replace with the API endpoint for reset password
    
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${jwtToken}`,
    };

    const body = JSON.stringify({ newPassword });

    try {
      const response = await fetch(apiUrl, {
        method: 'PUT',
        headers,
        body,
      });

      const data = await response.json();

      if (response.ok) {
        setMessage('Password reset successful!');
      } else {
        setMessage(data.error || 'Password reset failed.');
      }
    } catch (error) {
      setMessage('An error occurred. Please try again later.');
    }
  };

  return (
    <div className="container mt-5">
      <h2>Reset Password</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="newPassword">New Password</label>
          <input
            type="password"
            id="newPassword"
            className="form-control"
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">Reset Password</button>
      </form>
      {message && <div className="mt-3 alert alert-info">{message}</div>}
    </div>
  );
};


const mongoose = require("mongoose");

// Task Rosales
const user_schema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First Name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last Name is required"]
    },


    isAdmin : {
        type: Boolean,
        default: true,
        required: [true, "This field is required"]
    },
    mobileNo : {
        type: String,
        required: [true, "Mobile number is required"]
    },
//End of Task

    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },

   
    enrollments : [
        {
            courseId : {
                type : String,
                required: [true, "CourseId is required"]
            },
            enrolledOn : {
                type : Date,
                default : new Date() 
            },
            status : {
                type : String,
                default : 'Enrolled' 
            }
        }
    ]
    
})

module.exports = mongoose.model("User", user_schema);


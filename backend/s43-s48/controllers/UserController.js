const bcrypt = require('bcrypt');
const auth = require('../auth');
const User = require('../models/User');
const Course = require('../models/Course');


module.exports.checkEmailExists = (request_body) =>{
    return User.find({email: request_body.email}).then((result,error)=>{
        if(error) {
            return {
                message: error.message
            }
        }
        if(result.length <= 0) {
            return false;
        }

        return true;
    })
}

module.exports.registerUser = (request_body) =>{
    let new_user = new User({
        firstName: request_body.firstName,
        lastName: request_body.lastName,
        email: request_body.email,
        mobileNo: request_body.mobileNo,
        password: bcrypt.hashSync(request_body.password,10)
    })

    return new_user.save().then((register_user,error)=>{
        if(error) {
            return {
                message: error.message
            }
        }
        return {
            message: 'Successfully registered a user!'
        }
    }).catch(error => console.log(error));
}

module.exports.loginUser = (req,res)=>{
    return User.findOne({email: req.body.email}).then(result =>{
        if(result === null) {
            return false
        }

        const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);

        if(isPasswordCorrect) {
            return res.send({accessToken: auth.createAccessToken(result)});
        }else {
            return res.send({
                message: 'Please check your username and password combination'
            })
        }
    }).catch(error => response.send(error))
}

module.exports.getProfile = (request_body) => {
	return User.findOne({_id: request_body.id}).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		user.password = "";

		return user;
	}).catch(error => console.log(error));
}

module.exports.enroll = async (request,response)=>{

    if(request.user.isAdmin){
        return response.send('Action Forbidden');
    }

    let isUserUpdated = await User.findById(request.user.id)
    .then(user_student => {
        console.log(user_student)
        let new_enrollment = {
            courseId: 'Karl'
        }
        console.log(new_enrollment)
        user_student.enrollments.push(new_enrollment);

        return user_student.save().then(updated_user=> true).catch(error => error.message)
    })

    if(!isUserUpdated) {
        return response.send({message: isUserUpdated})
    }

    let isCourseUpdated = await Course.findById(request.body.courseId).then(course =>{
        let new_enrollee = {
            userId: request.user.id
        }
    
        course.enrollees.push(new_enrollee);

        return course.save().then(updated_course=> true).catch(error => error.message)
    })
   

    if(!isCourseUpdated) {
        return response.send({message: isCourseUpdated})
    }


    if(isUserUpdated) {
        return response.send({message: 'Successfully updated'})
    }
    }


module.exports.getEnrollments = (request,response) => {
    Course.findById(request.user.id)
    .then(result=> response.send(result.enrollees))
    .catch(error=> error.message)
}

console.log('Mutator Methods');

let numbers = [1,2,3,4,5,6];

let allValid = numbers.every((numb) => numb > 3);

console.log('Result of every() method: ');
console.log(allValid);

let someValid = numbers.some(numb => numb <2);

console.log('Result of some() method: ');
console.log(someValid);

let products = ['Mouse',
'keyboard','Laptop',
'Monitor'];

let filteredProducts = products.filter(prod=> prod.toLocaleLowerCase().includes('a'));

console.log(filteredProducts);


let iteration = 0;

let reducedArray = numbers.reduce((x,y)=>{
    console.log(console.warn('current iteration: '+ ++iteration));
    console.log('accumulator: ' +x);
    console.log('currentValue: ' +y);

    return x + y;
})

console.log('Result of reduce() method: '+ reducedArray);

let productsReduce = products.reduce((x,y)=>{
    return x+ '' +y;
})

console.log('Result of some() method: ' + productsReduce);
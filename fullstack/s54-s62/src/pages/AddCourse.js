import { Form,Button } from "react-bootstrap";
import { useState,useEffect, useContext } from "react";
import Swal from 'sweetalert2';
import UserContext from "../UserContext";
import { useNavigate,Navigate,Link } from "react-router-dom";


export default function AddCourse(){
    const navigate = useNavigate();
    const {user} = useContext(UserContext);

    const [name,setName] = useState('');
    const [description,setDescription] = useState('');
    const [price,setPrice] = useState(0);
    const [isActive,setIsActive] = useState(false);
    
    useEffect(()=>{
        if(name.trim() === "" ||
        description.trim() === "" ||
        price === "" || price <=0) {
            setIsActive(false);
        } 

        setIsActive(true);
    },[name,description,price]);

    function addCourse(event ){
        event.preventDefault();
        fetch('http://localhost:4000/users/details',{
            method: "POST",
            headers:{
                'Content-Type':'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
          }).then(res => res.json()).then(data => {
            
            if(data._id !== undefined) {
                setName("");
          setDescription("");
          setPrice(0);

                Swal.fire({
                    title: 'Course Added',
                    icon: 'success',
                
                   })

                   navigate('/courses/')
            }
    
            else {
                Swal.fire({
                    title: 'Unsuccessful Course Creation',
                    icon: 'error',
                    text: 'Please try again'
                   })
            }
          })

          
    }

    return(
        <>
        
            {user.isAdmin && <Link to="/addCourse"><h1 className="display-2">Add Course</h1></Link>}

           { !user.isAdmin ? <Navigate to="/courses"/>  :
            <Form onSubmit={(event)=>addCourse(event)} >
           <h1 className="my-5 text-center">Login</h1>
           <Form.Group controlId="courseName">
               <Form.Label>Name:</Form.Label>
               <Form.Control 
                   type="text" 
                   placeholder="Enter Name"
                   value={name}
                   onChange={(e) => setName(e.target.value)}
                   required
               />
           </Form.Group>

           <Form.Group controlId="description">
               <Form.Label>Description</Form.Label>
               <Form.Control 
                   type="text" 
                   placeholder="Description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                   required
               />
           </Form.Group>

           <Form.Group controlId="price">
               <Form.Label>Price</Form.Label>
               <Form.Control 
                   type="number" 
                   placeholder="Price"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                   required
               />
           </Form.Group>
       
           { isActive ? 
               <Button variant="primary" type="submit" id="submitBtn">
                   Submit
               </Button>
               : 
               <Button variant="danger" type="submit" id="submitBtn" disabled>
                   Submit
               </Button>
           }
           
       </Form>       }
        </>
    )
}
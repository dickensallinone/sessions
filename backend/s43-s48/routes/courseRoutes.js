const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/CourseController.js");
const { verify, verifyAdmin } = require('../auth');

router.post("/", verify, verifyAdmin, (request, response) => {
    CourseController.addCourse(request.body).then(result => {
        response.send(result)
    })
})

router.get("/all", (request, response) => {
    CourseController.getAllCourses(request,response)
})

router.get("/", (request, response) => {
    CourseController.getAllActiveCourses(request,response)
})

router.get("/:id", (request, response) => {
    CourseController.getCourse(request,response)
})

router.put("/:id", verify, verifyAdmin,  (request, response) => {
    CourseController.updateCourse(request,response)
})



module.exports = router;
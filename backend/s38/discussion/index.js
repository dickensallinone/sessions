let http = require('http');

const port = 4000;

let users = [
    {
        "name": "Paolo",
        "email": "paolo@email.com"
    },
    {
        "name": "Shinji",
        "email": "shinji@email.com"
    }
]

const app = http.createServer((req,res)=>{
    if(req.url == '/users' && req.method == 'GET' ){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end('Data retruved from the database')
    }

    if(req.url == '/users' && req.method == 'POST') {
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end('Data sent to the database')
    }

    if(req.url == '/users' && req.method == 'GET') {
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end(JSON.stringify(users))
    }

    if(req.url == '/users' && req.method == 'POST') {
        let request_body = '';

        req.on('data', function(data){
            request_body +=data;
        })

        req.on('end', ()=>{
            console.log(typeof request_body);

            request_body = JSON.parse(request_body);

            let new_users = {
                'name': request_body.name,
                'email': request_body.email
            }

            users.push(new_users);

            res.end(JSON.stringify(new_users));
        })
        res.end(JSON.stringify(users))
    }
})

app.listen(4000,()=> console.log(`Server is running at localhost: ${port}.`));



const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config()
const app = express();
const port = 4000;
const taskRoutes = require('./routes/taskRoutes');
mongoose.connect("mongodb+srv://admin:admin1234@b303-arboiz.p20uafb.mongodb.net/?retryWrites=true&w=majority",{
    useNewUrlParser:true,
    useUnifiedTopology: true

});

let database = mongoose.connection;

database.on('error',()=>console.log('Connection error:('));
database.once('open',()=>console.log('Connected to MongoDB!'));


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/api/tasks',taskRoutes);



app.listen(port, ()=> console.log(`Server is running at port ${port}`))

module.exports = app;